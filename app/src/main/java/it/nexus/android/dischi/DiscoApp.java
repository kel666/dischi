package it.nexus.android.dischi;

import android.app.Application;

/**
 * Created by kel666 on 20/04/17.
 */

public class DiscoApp extends Application {
    private Tunnel tunnel;
    @Override
    public void onCreate() {
        super.onCreate();
        tunnel=Tunnel.getInstance(this);
    }

    public Tunnel getTunnel() {
        return tunnel;
    }
}
