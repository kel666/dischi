package it.nexus.android.dischi;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

public class MainActivity extends Activity {

    public static String TAG="MainActivity";

    private EditText titolo;
    private EditText autore;
    private Button salva;
    private Button openrest;

    private Disco disco=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        Log.d(TAG, "Vista impostata");
        //
        initFields();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //
        Disco.load(this, okListener, koListener);
    }

    private class DiscoDeserialize implements TunnelDeserializeInterface {
        @Override
        public void tunnelCallback(Object model) {
            disco= (Disco) model;
            Log.d(TAG, "Disco recuperato="+disco);
            titolo.setText(disco.getTitolo());
            autore.setText(disco.getAutore());
        }
    }


    private Response.Listener<String> okListener=
            new TunnelListener(Disco.class, new DiscoDeserialize());

    private Response.ErrorListener koListener=new Response.ErrorListener(){
        @Override
        public void onErrorResponse (VolleyError error){
            Log.e(TAG, "Errore durante la chiamata", error);
        }
    };

    private void initFields() {
        titolo= (EditText) findViewById(R.id.titolo);
        autore= (EditText) findViewById(R.id.autore);
        salva= (Button) findViewById(R.id.salva);
        salva.setOnClickListener(new SalvaListener(this));
        openrest= (Button) findViewById(R.id.openrest);
        openrest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(view.getContext(), RestActivity.class);
                MainActivity.this.startActivity(i);
            }
        });
    }

    public EditText getTitolo() {
        return titolo;
    }

    public EditText getAutore() {
        return autore;
    }
}