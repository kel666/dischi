package it.nexus.android.dischi;

import android.content.Context;
import android.util.Log;

import com.android.volley.*;
import com.android.volley.toolbox.*;

/**
 * Created by kel666 on 20/04/17.
 */

public class Tunnel {
    public static final String URL_PREFIX="http://kel666.station:8080/rest";
    private Context context;
    private RequestQueue queue;
    private static Tunnel instance=null;

    public static String TAG="Tunnel";

    private Tunnel(Context context) {
        this.context=context;
        setupQueue();
    }

    public void addRequest(Request req) {
        queue.add(req);
    }

    public static Tunnel getInstance(Context context) {
        if (Tunnel.instance==null) {
            Tunnel.instance=new Tunnel(context);
        }
        return Tunnel.instance;
    }

    private void setupQueue() {
        // Instantiate the cache
        Cache cache = new DiskBasedCache(context.getCacheDir(), 1024 * 1024); // 1MB cap

        // Set up the network to use HttpURLConnection as the HTTP client.
        Network network = new BasicNetwork(new HurlStack());

        // Instantiate the RequestQueue with the cache and network.
        queue = new RequestQueue(cache, network);

        // Start the queue
        queue.start();
    }

    public static String buildUrl(String resource, Integer id) {
        String url=URL_PREFIX+resource+(id!=null ? "/"+id : "");
        Log.d(TAG, "buildUrl="+url);
        return url;
    }

    public void getRequest(String resource, Integer id,
                           Response.Listener<String> okListener,
                           Response.ErrorListener koListener) {
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                buildUrl(resource, id),
                okListener, koListener);
        queue.add(stringRequest);
    }
}
