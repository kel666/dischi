package it.nexus.android.dischi.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;

import java.util.List;

import it.nexus.android.dischi.Disco;
import it.nexus.android.dischi.R;
import it.nexus.android.dischi.Tunnel;

/**
 * Created by kel666 on 21/04/17.
 */

public class RestFragment extends Fragment {
    private TextView output;
    private List<Disco> dischi;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View ret=inflater.inflate(R.layout.rest, container, false);
        output= (TextView) ret.findViewById(R.id.output);
        output.setText(dischi.toString());
        return ret;
    }

    public void setDischi(List<Disco> dischi) {
        this.dischi = dischi;
    }
}