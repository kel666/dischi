package it.nexus.android.dischi.fragments;

import android.content.Context;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;
import java.util.Vector;

import it.nexus.android.dischi.Disco;
import it.nexus.android.dischi.R;

/**
 * Created by kel666 on 21/04/17.
 */

public class DischiListFragment extends Fragment {
    private ListView listView;
    private List<Disco> dischi=new Vector<Disco>();
    private List<Disco> selected=new Vector<Disco>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View ret=inflater.inflate(R.layout.list, container, false);
        listView= (ListView) ret.findViewById(R.id.lista);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        listView.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
            @Override
            public void onItemCheckedStateChanged(ActionMode actionMode, int pos, long id, boolean checked) {
                Disco disco=(Disco) listView.getItemAtPosition(pos);
                View selView=listView.getChildAt(pos);
                if (checked) {
                    selView.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                    selected.add(disco);
                } else {
                    selView.setBackgroundColor(getResources().getColor(R.color.white));
                    selected.remove(disco);
                }
                selView.invalidate();
            }

            @Override
            public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
                MenuInflater inflater = actionMode.getMenuInflater();
                inflater.inflate(R.menu.dischi, menu);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
                Disco disco= (Disco) listView.getSelectedItem();
                switch(menuItem.getItemId()) {
                    case R.id.delete:
                        for(int i=0; i<selected.size(); i++)
                            selected.get(i).delete();
                        break;
                    case R.id.modify:
                        if (selected.size()==1)
                            disco.save(getContext());
                        break;
                }
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode actionMode) {

            }
        });
        DischiAdapter adapter=new DischiAdapter();
        listView.setAdapter(adapter);
        return ret;
    }

    public void setDischi(List<Disco> dischi) {
        this.dischi = dischi;
    }

    private class DischiAdapter implements ListAdapter {

        @Override
        public boolean areAllItemsEnabled() {
            return true;
        }

        @Override
        public boolean isEnabled(int i) {
            return true;
        }

        @Override
        public void registerDataSetObserver(DataSetObserver dataSetObserver) {
        }

        @Override
        public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {
        }

        @Override
        public int getCount() {
            return dischi.size();
        }

        @Override
        public Object getItem(int i) {
            return dischi.get(i);
        }

        @Override
        public long getItemId(int i) {
            return dischi.get(i).getId();
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            Disco disco= (Disco) getItem(i);
            LayoutInflater inflater = (LayoutInflater)getContext().getSystemService
                    (Context.LAYOUT_INFLATER_SERVICE);
            View item=inflater.inflate(R.layout.listitem, viewGroup, false);
            ((TextView)item.findViewById(R.id.autore)).setText(disco.getAutore());
            ((TextView)item.findViewById(R.id.titolo)).setText(disco.getTitolo());
            return item;
        }

        @Override
        public int getItemViewType(int i) {
            return 0;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        @Override
        public boolean isEmpty() {
            return dischi.isEmpty();
        }
    }
}
