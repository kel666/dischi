package it.nexus.android.dischi;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;

/**
 * Created by kel666 on 19/04/17.
 */

public class SalvaListener implements View.OnClickListener {
    public static String TAG="SalvaListener";
    private Context context;

    public SalvaListener(Context context) {
        this.context=context;
    }

    @Override
    public void onClick(View view) {
        if (context instanceof MainActivity) {
            MainActivity ma=(MainActivity)context;
            Disco disco=new Disco();
            disco.setAutore(ma.getAutore().getText().toString());
            disco.setTitolo(ma.getTitolo().getText().toString());
            disco.save(context);
        }
        Log.d(TAG, "Salvato!");
    }
}