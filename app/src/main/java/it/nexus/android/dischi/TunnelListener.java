package it.nexus.android.dischi;

import com.android.volley.Response;

/**
 * Created by kel666 on 20/04/17.
 */

public class TunnelListener implements Response.Listener<String> {
    private Class modelClass;
    private TunnelDeserializeInterface tdi;

    public TunnelListener(Class modelClass, TunnelDeserializeInterface tdi) {
        this.modelClass=modelClass;
        this.tdi=tdi;
    }

    @Override
    public void onResponse(String response) {
        Object obj=null;
        if (modelClass==Disco.class) {
            obj=Disco.deserialize(response);
        }
        tdi.tunnelCallback(obj);
    }
}
