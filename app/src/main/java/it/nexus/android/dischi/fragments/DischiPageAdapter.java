package it.nexus.android.dischi.fragments;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.view.View;

import java.util.List;

import it.nexus.android.dischi.Disco;

/**
 * Created by kel666 on 21/04/17.
 */

public class DischiPageAdapter extends FragmentPagerAdapter {
    public Fragment fragment[]=new Fragment[2];

    public DischiPageAdapter(FragmentManager fm, List<Disco> dischi) {
        super(fm);
        fragment[0]=new RestFragment();
        ((RestFragment)fragment[0]).setDischi(dischi);
        fragment[1]=new DischiListFragment();
        ((DischiListFragment)fragment[1]).setDischi(dischi);
    }

    @Override
    public int getCount() {
        return fragment.length;
    }

    @Override
    public Fragment getItem(int position) {
        return fragment[position];
    }
}
