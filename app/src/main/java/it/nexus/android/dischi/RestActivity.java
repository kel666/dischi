package it.nexus.android.dischi;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.ContextMenu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.w3c.dom.Text;

import it.nexus.android.dischi.fragments.DischiPageAdapter;

public class RestActivity extends FragmentActivity {

    private ViewPager pager;
    private Button tab1;
    private Button tab2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rest_act);
        pager= (ViewPager) findViewById(R.id.pager);
        tab1= (Button) findViewById(R.id.tab1);
        tab1.setOnClickListener(new TabClickListener(0));
        tab2= (Button) findViewById(R.id.tab2);
        tab2.setOnClickListener(new TabClickListener(1));
        getData();
    }

    private void getData() {
        JsonArrayRequest jar = new JsonArrayRequest
                (Request.Method.GET,
                        Tunnel.buildUrl("/Disco", null)
                        , null, new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        pager.setAdapter(
                                new DischiPageAdapter(
                                        getSupportFragmentManager(),
                                        Disco.deserialize(response)));
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {


                    }
                });
        Tunnel.getInstance(this).addRequest(jar);
    }

    private class TabClickListener implements View.OnClickListener {
        private int pos;
        public TabClickListener(int pos) {
            this.pos=pos;
        }
        @Override
        public void onClick(View view) {
            pager.setCurrentItem(pos);
        }
    }
}
