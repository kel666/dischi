package it.nexus.android.dischi;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.android.volley.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Vector;

/**
 * Created by kel666 on 19/04/17.
 */

public class Disco {
    public static String TAG="Disco";

    private int id;
    private String autore="";
    private String titolo="";

    @Override
    public String toString() {
        String ret=null;
        JSONObject obj=new JSONObject();
        try {
            obj.put("id", id);
            obj.put("autore", autore);
            obj.put("titolo", titolo);
            ret=obj.toString();
        } catch (JSONException e) {
            Log.e(TAG, "toString", e);
        }
        return ret;
    }

    public void delete() {
        Log.d(TAG, "Disco "+this+" eliminato");
    }

    public void save(Context context) {
        Log.d(TAG, "Salvando Disco "+this);
        String sharedName=context.getResources().getString(R.string.app_name);
        SharedPreferences prefs=context.getSharedPreferences(sharedName, Context.MODE_PRIVATE);
        prefs.edit().putString(Disco.TAG, toString())
                .commit();
    }

    public static void load(Context context,
                             Response.Listener<String> okListener,
                             Response.ErrorListener koListener) {
        Log.d(TAG, "Recuperando Disco...");
        DiscoApp app=(DiscoApp)context.getApplicationContext();
        Tunnel tunnel=app.getTunnel();
        tunnel.getRequest("/"+TAG, 3, okListener, koListener);
        /*
        String sharedName=context.getResources().getString(R.string.app_name);
        SharedPreferences prefs=context.getSharedPreferences(sharedName, Context.MODE_PRIVATE);
        String discoJson=prefs.getString(Disco.TAG, null);
        Disco disco=deserialize(discoJson);
        Log.d(TAG, "Disco recuperato="+disco);
        return disco;
        */
    }

    public static Disco deserialize(String json) {
        Disco disco=new Disco();
        try {
            JSONObject obj=new JSONObject(json);
            disco.setAutore(obj.getString("autore"));
            disco.setTitolo(obj.getString("titolo"));
            disco.setId(obj.getInt("id"));
        } catch (JSONException e) {
            Log.e(TAG, "onResume", e);
        }
        return disco;
    }

    public String getAutore() {
        return autore;
    }

    public void setAutore(String autore) {
        this.autore = autore;
    }

    public String getTitolo() {
        return titolo;
    }

    public void setTitolo(String titolo) {
        this.titolo = titolo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public static List<Disco> deserialize(JSONArray discs) {
        Vector<Disco> dischi=new Vector<Disco>();
        for(int i=0; i<discs.length(); i++) {
            try {
                Disco disc=Disco.deserialize(
                        discs.getJSONObject(i).toString());
                dischi.add(disc);
            } catch (JSONException e) {
                Log.e(TAG, "errore nel parsing", e);
            }
        }
        return dischi;
    }
}
