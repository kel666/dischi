package it.nexus.android.dischi;

import android.util.Log;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class DiscoTest {
    @Test
    public void deserializeTest() throws Exception {
        String tit="Sgt.Pepper";
        String aut="Beatles";
        String json="{\"titolo\":\""+tit+"\", \"autore\":\""+aut+"\"}";
        Disco disco=Disco.deserialize(json);
        System.out.println("disco="+disco);
        boolean titOk=(disco.getTitolo().equals(tit));
        boolean autOk=(disco.getAutore().equals(aut));
        assertTrue(titOk && autOk);
    }

    @Test
    public void serializeTest() throws Exception {
        Disco disco=new Disco();
        disco.setAutore("Beatles");
        disco.setTitolo("Sgt.Pepper");
        assertEquals("{\"titolo\":\"Sgt.Pepper\",\"autore\":\"Beatles\"}",
                disco.toString());
    }
}